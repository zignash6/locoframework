
Pod::Spec.new do |spec|
  spec.name         = "LocoFramework"
  spec.version      = "1.0.0"
  spec.summary      = "This is sample locoframework for test"
  spec.description  = <<-DESC
                        Sample LocaFramework, This is test examples
                   DESC
  spec.homepage     = "https://zignash@bitbucket.org/zignash6/locoframework"
  spec.license      = { :type => 'MIT', :text => <<-LICENSE
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  LICENSE
  }
  spec.author       = { "undrakonda_krishna" => "undrakonda.krishna@ivycomptech.com" }
  spec.platform     = :ios, "11.0"
  spec.swift_version = '5.0'
  spec.source       = { :git => "https://zignash@bitbucket.org/zignash6/locoframework.git", :tag => "1.0.0" }
  spec.source_files  = "LocoFramework/**/*.{h,m,swift}"
  

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"

end
