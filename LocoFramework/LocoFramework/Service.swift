//
//  Service.swift
//  LocoFramework
//
//  Created by Undrakonda Gopala Krishna on 08/02/21.
//  Copyright © 2021 Undrakonda Gopala Krishna. All rights reserved.
//

import Foundation

public class Service {
    
    private init() {
        
    }
    
    public static func doSomethings() -> String {
        return "Do Something....!"
    }
}
